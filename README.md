# Golang tapi kaya laravel-lumen

Terinspirasi dari Laragol (https://github.com/laragol/laragol). Percobaan saya untuk lebih memahami konsep golang dengan menggunakan metode yang sudah ada pada lumen.

### Packages

* Reflex untuk Makefile[(cespare/reflex)](https://github.com/cespare/reflex)
* Http Server [(valyala/fasthttp)](https://github.com/valyala/fasthttp)
* Routing [(qiangxue/fasthttp-routing)](https://github.com/qiangxue/fasthttp-routing)
* ORM [(jinzhu/gorm)](https://github.com/jinzhu/gorm)

## Cara Penggunaan

1. Modifikasi file Connection.go sesuai dengan setting MySQL.

2. 
```bash
make install
make build-dev
./bin/main migrate
./bin/main db:seed
make dev #linux
make dev-mac #mac
```

## Contoh
![Skrinsut](skrinsut.png)
