package Migrations

import (
	"clean-go/src/app/Models"
	"fmt"

	"clean-go/src/framework/Database/Connection"
)

type CreateDivisionsTable struct {
}

func (CreateDivisionsTable) Up() {
	db := Connection.Get()
	
	db.Exec("SET FOREIGN_KEY_CHECKS=0;")
	db.DropTable(&Models.Division{})
	db.CreateTable(&Models.Division{})
}

func (CreateDivisionsTable) Down() {
	fmt.Println("CreateDivisionsTable::down Not Implemented.")
}
