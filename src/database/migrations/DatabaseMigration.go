package Migrations

import . "clean-go/src/framework/Database/Migration"

func Migrate() {
	Register(CreateDivisionsTable{})
	Register(CreatePositionTable{})
	Register(CreateUsersTable{})
}
