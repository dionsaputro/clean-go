package Migrations

import (
	"clean-go/src/app/Models"
	"fmt"

	"clean-go/src/framework/Database/Connection"
)

type CreatePositionTable struct {
}

func (CreatePositionTable) Up() {
	db := Connection.Get()
	
	db.Exec("SET FOREIGN_KEY_CHECKS=0;")
	db.DropTable(&Models.Position{})
	db.CreateTable(&Models.Position{})
}

func (CreatePositionTable) Down() {
	fmt.Println("CreatePositionTable::down Not Implemented.")
}
