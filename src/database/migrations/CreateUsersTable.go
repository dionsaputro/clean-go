package Migrations

import (
	"clean-go/src/app/Models"
	"fmt"

	"clean-go/src/framework/Database/Connection"
)

type CreateUsersTable struct {
}

func (CreateUsersTable) Up() {
	db := Connection.Get()
	db.Exec("SET FOREIGN_KEY_CHECKS=0;")
	db.DropTable(&Models.User{})
	db.CreateTable(&Models.User{})
	db.Model(&Models.User{}).AddForeignKey("division_id", "divisions(id)", "RESTRICT", "RESTRICT")
}

func (CreateUsersTable) Down() {
	fmt.Println("CreateUsersTable::down Not Implemented.")
}
