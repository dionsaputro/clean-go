package Seeds

import (
	"clean-go/src/app/Models"

	"clean-go/src/framework/Database/Connection"
)

type UsersTableSeeder struct {
}

func (UsersTableSeeder) Run() {
	db := Connection.Get()
	db.Create(&Models.User{Name: "Rio", Email: "rio@mail.com", DivisionID: 1})
	db.Create(&Models.User{Name: "Edgar", Email: "edgar@mail.com", DivisionID: 2})
	db.Create(&Models.User{Name: "Ario", Email: "ario@mail.com", DivisionID: 3})
}
