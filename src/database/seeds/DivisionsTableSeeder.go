package Seeds

import (
	"clean-go/src/app/Models"

	"clean-go/src/framework/Database/Connection"
)

type DivisionsTableSeeder struct {
}

func (DivisionsTableSeeder) Run() {
	db := Connection.Get()
	db.Create(&Models.Division{Name: "Head Office", PositionID: 1})
	db.Create(&Models.Division{Name: "HR Staff", PositionID: 2})
	db.Create(&Models.Division{Name: "Finance", PositionID: 3})
	db.Create(&Models.Division{Name: "Accounting", PositionID: 5})
	db.Create(&Models.Division{Name: "Research And Development", PositionID: 4})
}
