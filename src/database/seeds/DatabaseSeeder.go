package Seeds

import . "clean-go/src/framework/Database/Seeder"

func Run() {
	Call(PositionTableSeeder{})
	Call(DivisionsTableSeeder{})
	Call(UsersTableSeeder{})
}
