package Seeds

import (
	"clean-go/src/app/Models"

	"clean-go/src/framework/Database/Connection"
)

type PositionTableSeeder struct {
}

func (PositionTableSeeder) Run() {
	db := Connection.Get()
	db.Create(&Models.Position{Name: "Director"})
	db.Create(&Models.Position{Name: "HR"})
	db.Create(&Models.Position{Name: "Financial Analyst"})
	db.Create(&Models.Position{Name: "Research and Development"})
	db.Create(&Models.Position{Name: "Staff"})
}
