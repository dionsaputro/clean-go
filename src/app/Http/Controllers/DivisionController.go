package Controllers

import (
	"clean-go/src/app/Models"
	"encoding/json"
	"strconv"

	"clean-go/src/framework/Database/Connection"
	"clean-go/src/framework/Response"
	"github.com/qiangxue/fasthttp-routing"
)

type DivisionController struct {
}

func (DivisionController) Index(c *routing.Context) error {
	db := Connection.Get()
	var division []Models.Division
	db.Preload("Position").Find(&division)
	return Response.Json(c, division)
}

func (DivisionController) Show(c *routing.Context) error {
	db := Connection.Get()
	var division Models.Division
	db.Find(&division, c.Param("divisionId"))
	return Response.Json(c, division)
}

func (DivisionController) Store(c *routing.Context) error {
	db := Connection.Get()
	body := c.PostBody()
	division := &Models.Division{}
	json.Unmarshal(body, division)
	db.Save(&division)
	return Response.Json(c, division)
}

func (DivisionController) Update(c *routing.Context) error {
	db := Connection.Get()
	body := c.PostBody()
	division := &Models.Division{}
	json.Unmarshal(body, division)
	divisionId, _ := strconv.ParseUint(c.Param("divisionId"), 10, 64)
	division.ID = divisionId
	db.Save(&division)
	return Response.Json(c, division)
}
