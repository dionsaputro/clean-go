package Routes

import (
	. "clean-go/src/app/Http/Controllers"
	_routing "github.com/qiangxue/fasthttp-routing"
)

func Handle(router *_routing.Router) {
	api := router.Group("/api")
	api.Get("/", UserController{}.Index)
	api.Get("/users", UserController{}.Index)
	api.Get("/users/<userId>", UserController{}.Show)
	api.Post("/users", UserController{}.Store)
	api.Put("/users/<userId>", UserController{}.Update)

	api.Get("/divisions", DivisionController{}.Index)
}
