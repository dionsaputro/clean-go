package Models

type User struct {
	ID    uint64 `json:"id"      gorm:"primary_key"`
	Name  string `json:"name"    gorm:"size:100"`
	Email string `json:"email"   gorm:"size:100"`
	Division   Division `json:"division"`
	DivisionID uint64  `json:"-"`
}
