package Models

type Division struct {
	ID   uint64 `json:"id"   gorm:"primary_key"`
	Name string `json:"name" gorm:"size:100"`
	Position   Position `json:"position"`
	PositionID uint64  `json:"-"`
}
