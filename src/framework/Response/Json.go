package Response

import (
	"clean-go/src/app/Helpers"
	"fmt"

	"encoding/json"

	"github.com/qiangxue/fasthttp-routing"
)

func Json(c *routing.Context, v interface{}) (err error) {
	fmt.Println(v)
	b, err := json.Marshal(v)

	if err != nil {
		Helpers.Error(c, err)
		return
	}

	fmt.Fprintf(c, string(b))

	return
}
